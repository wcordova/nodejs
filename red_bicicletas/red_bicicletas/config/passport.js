const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');
const FacebookStrategy = require('passport-facebook').Strategy;


passport.use(new FacebookTokenStrategy({
  clientID: '469369737751312',
  clientSecret: 'f85e482f203cf758d9014e1d235119d1'},
  function(accessToken, refreshToken, profile, done){
      try{
          Usuario.findOrCreateByFacebook(profile, function(err, user){
              if(err){
                  console.log(err);
                  return done(err, user);
              }
              return done(err, user);
          });

      }catch(err2){
          console.log(err2);
          return done(err2, null);
      }
  })
);


passport.use(new LocalStrategy(
  function(email, password, done){
      Usuario.findOne({email: email}, function(err, usuario){
          if(err) return done(err)
          if(!usuario) return done(null, false, {message: 'Email no existente o incorrecto'});
          if (!usuario.validPassword(password)) return done(null, false, {message: 'Password incorrecto'});

          return done(null, usuario)
      })
  }
));
passport.use(new GoogleStrategy({
  clientID: '224956252759-c50pscm3m2mnc1npv2qdd8qdj7bsiiah.apps.googleusercontent.com',
  clientSecret: 'rxA7B78riPPM4vcTPaotWYVM',
  callbackURL: "/auth/google/callback"
},
function(accessToken, refreshToken, profile, done) {
     User.findOrCreate({ googleId: profile.id }, function (err, user) {
       return done(err, user);
     });
}
));


passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  Usuario.findById(id, (err, usuario) => {
      cb(err, usuario);
  });
});

module.exports = passport;